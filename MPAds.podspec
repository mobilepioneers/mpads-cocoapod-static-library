Pod::Spec.new do |s|
  s.name           = 'MPAds'
  s.version        = '0.0.76'
  s.summary        = 'MPAds static library'
  s.homepage       = 'https://bitbucket.org/mobilepioneers/mpads-cocoapod-static-library'
  s.authors        = {'Bob de Graaf' => 'graafict@gmail.com'}
  s.source         = { :git => 'https://bitbucket.org/mobilepioneers/mpads-cocoapod-static-library.git', :tag => '0.0.76' }
  s.preserve_paths = 'libMPAds.a'
  s.license 	   = 'MIT' 
  s.source_files   = '**/*.{h,m}'
  s.frameworks 	    = 'StoreKit', 'CoreLocation', 'MessageUI', 'OpenAl', 'AVFoundation', 'CFNetwork', 'CoreMotion', 'CoreMedia', 'EventKit', 'EventKitUI', 'CoreTelephony', 'MediaPlayer', 'AudioToolbox', 'MobileCoreServices', 'Security', 'iAd', 'AssetsLibrary', 'SystemConfiguration', 'QuartzCore', 'AddressBookUI', 'AddressBook', 'PassKit', 'Twitter'
  s.weak_frameworks= 'AdSupport'
  s.library 	    = 'MPAds', 'z', 'c++', 'sqlite3'
  s.platform       = :ios, '7.0'
  s.requires_arc   = true
  s.xcconfig 	    = { 'OTHER_LDFLAGS' => '-lObjC', 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/MPAds"' }
  s.dependency 'BDGMacros'
  s.dependency 'BDGUtilities'
  s.dependency 'Reachability'
  s.dependency 'UIDeviceAddition'
end
