//
//  AdSpecs.h
//  TVGiDS
//
//  Created by Bob de Graaf on 08-08-11.
//  Copyright 2011 Mobile Pioneers. All rights reserved.
//

#import "MPAdsConstants.h"

#import <Foundation/Foundation.h>

//AdTypes
typedef enum {
    kAdTypeWeb=0,
    kAdTypeAdmob=1,
} AdType;

@interface AdSpecs : NSObject
{

}

//General
@property(nonatomic) AdType adType;
@property(nonatomic,strong) NSString *adID;
@property(nonatomic,strong) NSString *adURL;
@property(nonatomic,strong) NSString *adTitle;
@property(nonatomic,strong) NSString *mailBody;
@property(nonatomic,strong) NSString *bannerURL;
@property(nonatomic,strong) NSString *appStoreID;
@property(nonatomic,strong) NSString *admobID;
@property(nonatomic,strong) NSString *bannerTitle;
@property(nonatomic,strong) NSString *mailSubject;
@property(nonatomic,strong) NSString *sectionTitle;
@property(nonatomic,strong) NSString *videoThumbURL;
@property(nonatomic,strong) NSString *mailToAddress;
@property(nonatomic,strong) NSString *mailSentPopupText;
@property(nonatomic,strong) NSString *mailOpenMeasureURL;
@property(nonatomic,strong) NSString *mailSentMeasureURL;
@property(nonatomic,strong) NSString *measureSeeURL;
@property(nonatomic,strong) NSString *measureClickURL;
@property(nonatomic,strong) NSString *imageAdImageURL;
@property(nonatomic,strong) NSString *box2DImageURL;
@property(nonatomic,strong) NSString *box2DPopupImageURL;

@property(nonatomic) int interval;
@property(nonatomic) int adAnim;
@property(nonatomic) int siteID;
@property(nonatomic) int adRow;
@property(nonatomic) int adSection;
@property(nonatomic) int adWidth1;
@property(nonatomic) int adHeight;
@property(nonatomic) int adHeight0;
@property(nonatomic) int adHeight1;
@property(nonatomic) int adHeight3;
@property(nonatomic) int adDelay;
@property(nonatomic) bool active;
@property(nonatomic) bool overlay;
@property(nonatomic) bool IAPPossible;
@property(nonatomic) bool videoWebview;
@property(nonatomic) bool directAppStore;

//Video Ad
@property(nonatomic) int soundOn;
@property(nonatomic) int numberOfTimes;
@property(nonatomic) int delayBetweenAds;
@property(nonatomic) int timeAfterVideoIsPlayed;

//Box2D Ad
@property(nonatomic)float angle;
@property(nonatomic)float startX;
@property(nonatomic)float startY;
@property(nonatomic)float gravity;
@property(nonatomic)float density;
@property(nonatomic)float friction;
@property(nonatomic)float restitution;
@property(nonatomic,strong) UIImage *box2DImage;
@property(nonatomic,strong) UIImage *popupImage;

@end
