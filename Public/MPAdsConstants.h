//
//  Constants.h
//  PullToRefresh
//
//  Created by Bob de Graaf on 23-09-12.
//  Copyright (c) 2012 GraafICT. All rights reserved.
//

#import <BDGMacros.h>
#import <BDGUtilities/InAppStore.h>
#import <BDGUtilities/InAppPurchase.h>
#import <BDGUtilities/LocationGetter.h>

#import "MPAds.h"

//Singletons
#define mpAds           [MPAds sharedMPAds]
#define mpIAStore       [InAppStore sharedInAppStore]
#define mpIAPurchase    [InAppPurchase sharedInAppPurchase]
#define locGetter       [LocationGetter sharedLocationGetter]

//Notifications
#define kMPAdsRefreshNotification               @"kMPAdsRefreshNotification"
#define kMPAdsPopupAppearedNotification         @"kMPAdsPopupAppearedNotification"
#define kMPAdsPopupDisappearedNotification      @"kMPAdsPopupDisappeardNotification"
#define kMPAdsNoAdsIAPPurchasedNotification     @"kMPAdsNoAdsIAPPurchasedNotification"

//GPS Keys
#define kGPSTimesAsked                          @"kGPSTimesAsked"
#define kGPSLastRefresh                         @"kGPSLastRefresh"
#define kGPSLastLatitude                        @"kLastGPSLatitude"
#define kGPSLastLongitude                       @"kLastGPSLongitude"
#define kGPSPermissionChanged                   @"kGPSPermissionChanged"
#define kGPSPermissionGranted                   @"kGPSPermissionGranted"
#define kMPAdsGPSApplePopupAnswered             @"kMPAdsGPSApplePopupAnswered"
#define kMPAdsGPSCustomPopupAnswered            @"kMPAdsGPSCustomPopupAnswered"
#define kMPAdsGPSLocationUpdated                @"kMPAdsGPSLocationUpdated"

//IAP Keys
#define kPurchasedNoAds                         @"kPurchasedNoMPAds"