//
//  NavigationbarDynamicItem.h
//  MPAds
//
//  Created by Bob de Graaf on 01-11-17.
//  Copyright © 2017 MobilePioneers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface NavigationbarDynamicItem : NSObject
{
    
}

@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *urlStr;

@end
