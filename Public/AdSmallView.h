//
//  AdSmallView.h
//
//  Created by Bob de Graaf on 09-01-11.
//  Copyright 2011 Mobile Pioneers. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AdSpecs.h"

@class AdSmallView;

@protocol AdSmallViewDelegate <NSObject>
@optional
-(void)MPAdsExitAd:(AdSmallView *)adSmallView;
-(void)MPAdsWillCloseAd:(AdSmallView *)adSmallView;
-(void)MPAdsWillNotLoadAd:(AdSmallView *)adSmallView;
-(void)MPAdsVideoDidFinish:(AdSmallView *)adSmallView;
-(void)MPAdsAdLoadCompleted:(AdSmallView *)adSmallView;
-(void)MPAdsDidEndFullScreen:(AdSmallView *)adSmallView;
-(void)MPAdsWillEndFullScreen:(AdSmallView *)adSmallView;
-(void)MPAdsWillStartFullScreen:(AdSmallView *)adSmallView;
-(void)MPAdsDidReceiveInterstitial:(AdSmallView *)adSmallView;
-(void)MPAdsDidResizeAd:(AdSmallView *)adSmallView finalWidth:(CGFloat)finalWidth finalHeight:(CGFloat)finalHeight;
@end

@interface AdSmallView : UIView
{
       
}

//Booleans
@property(nonatomic) bool canExit;
@property(nonatomic) bool interstitialReady;
@property(nonatomic) bool iAdFallBackToAdMob;

//Floats
@property(nonatomic) float adWidth;
@property(nonatomic) float adHeight;

//AdType
@property(nonatomic) AdType currentAdType;

//Content URL
@property(nonatomic,strong) NSString *contentURL;

//Strong
@property(nonatomic,strong) AdSpecs *specs;
@property(nonatomic,strong) NSString *adMobID;
@property(nonatomic,strong) UIViewController *parent;
@property(nonatomic,strong) NSArray *admobTestDevices;
@property(nonatomic,strong) NSMutableDictionary *headersDict;
@property(nonatomic,assign) id <AdSmallViewDelegate> delegate;

-(void)clearAds;
-(void)reloadURL;
-(void)measureAd;
-(BOOL)showInterstitial;
-(void)loadAd:(NSString *)viewName;
-(id)initWithFrame:(CGRect)frame parent:(UIViewController *)parent;

@end
